package ru.t1.zkovalenko.tm.api;

public interface IProjectController {

    void createProject();

    void clearProjects();

    void showProjects();

}
