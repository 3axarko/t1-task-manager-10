package ru.t1.zkovalenko.tm.api;

import ru.t1.zkovalenko.tm.model.Task;

import java.util.List;

public interface ITasktService {

    List<Task> findAll();

    Task add(Task project);

    Task create(String name);

    Task create(String name, String description);

    void clear();

}
