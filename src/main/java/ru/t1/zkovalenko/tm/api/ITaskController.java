package ru.t1.zkovalenko.tm.api;

public interface ITaskController {

    void createTask();

    void clearTasks();

    void showTasks();

}
