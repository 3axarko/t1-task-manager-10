package ru.t1.zkovalenko.tm.api;

import ru.t1.zkovalenko.tm.model.Project;

import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    Project add(Project project);

    Project create(String name);

    Project create(String name, String description);

    void clear();

}
