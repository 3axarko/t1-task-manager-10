package ru.t1.zkovalenko.tm.api;

import ru.t1.zkovalenko.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    Task add(Task project);

    void clear();

}
