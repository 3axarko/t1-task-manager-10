package ru.t1.zkovalenko.tm.service;

import ru.t1.zkovalenko.tm.api.IProjectRepository;
import ru.t1.zkovalenko.tm.api.IProjectService;
import ru.t1.zkovalenko.tm.constant.ConsoleColorConst;
import ru.t1.zkovalenko.tm.model.Project;
import ru.t1.zkovalenko.tm.util.ColorizeConsoleTextUtil;

import java.util.List;

public final class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public Project add(Project project) {
        if (project == null) return null;
        return projectRepository.add(project);
    }

    @Override
    public Project create(final String name){
        if (name == null || name.isEmpty()) return null;
        return add (new Project(name));
    }

    @Override
    public Project create(final String name, String description){
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) {
            description = ColorizeConsoleTextUtil.colorText("Have no description", ConsoleColorConst.YELLOW);
        }
        return add (new Project(name, description));
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

}
