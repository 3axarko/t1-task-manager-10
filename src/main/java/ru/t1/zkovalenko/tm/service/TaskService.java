package ru.t1.zkovalenko.tm.service;

import ru.t1.zkovalenko.tm.api.ITaskRepository;
import ru.t1.zkovalenko.tm.api.ITasktService;
import ru.t1.zkovalenko.tm.constant.ConsoleColorConst;
import ru.t1.zkovalenko.tm.model.Task;
import ru.t1.zkovalenko.tm.util.ColorizeConsoleTextUtil;

import java.util.List;

public final class TaskService implements ITasktService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public Task add(Task project) {
        if (project == null) return null;
        return taskRepository.add(project);
    }

    @Override
    public Task create(final String name){
        if (name == null || name.isEmpty()) return null;
        return add (new Task(name));
    }

    @Override
    public Task create(final String name, String description){
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) {
            description = ColorizeConsoleTextUtil.colorText("Have no description", ConsoleColorConst.YELLOW);
        }
        return add (new Task(name, description));
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

}
