package ru.t1.zkovalenko.tm.controller;

import ru.t1.zkovalenko.tm.api.IProjectController;
import ru.t1.zkovalenko.tm.api.IProjectService;
import ru.t1.zkovalenko.tm.constant.ConsoleColorConst;
import ru.t1.zkovalenko.tm.model.Project;
import ru.t1.zkovalenko.tm.util.ColorizeConsoleTextUtil;
import ru.t1.zkovalenko.tm.util.TerminalUtil;

import java.util.List;

public final class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void createProject(){
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();

        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();

        final Project project = projectService.create(name, description);
        if (project == null) System.err.println("[ERROR]\nName is required field");
        else ColorizeConsoleTextUtil.colorPrintln("[OK]", ConsoleColorConst.GREEN);
    }

    @Override
    public void clearProjects(){
        System.out.println("[CLEAR PROJECTS]");
        projectService.clear();
        ColorizeConsoleTextUtil.colorPrintln("[OK]", ConsoleColorConst.GREEN);
    }

    @Override
    public void showProjects(){
        System.out.println("[PROJECT LIST]");
        final List<Project> projects = projectService.findAll();
        int index = 1;
        for(final Project project: projects){
            System.out.println(index + ". " + project);
            index++;
        }
        ColorizeConsoleTextUtil.colorPrintln("[OK]", ConsoleColorConst.GREEN);
    }

}
