package ru.t1.zkovalenko.tm.controller;

import ru.t1.zkovalenko.tm.api.ITaskController;
import ru.t1.zkovalenko.tm.api.ITasktService;
import ru.t1.zkovalenko.tm.constant.ConsoleColorConst;
import ru.t1.zkovalenko.tm.model.Task;
import ru.t1.zkovalenko.tm.util.ColorizeConsoleTextUtil;
import ru.t1.zkovalenko.tm.util.TerminalUtil;

import java.util.List;

public final class TaskController implements ITaskController {

    private final ITasktService tasktService;

    public TaskController(final ITasktService tasktService) {
        this.tasktService = tasktService;
    }

    @Override
    public void createTask(){
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();

        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();

        final Task task = tasktService.create(name, description);
        if (task == null) System.err.println("[ERROR]\nName is required field");
        else ColorizeConsoleTextUtil.colorPrintln("[OK]", ConsoleColorConst.GREEN);
    }

    @Override
    public void clearTasks(){
        System.out.println("[CLEAR TASK]");
        tasktService.clear();
        ColorizeConsoleTextUtil.colorPrintln("[OK]", ConsoleColorConst.GREEN);
    }

    @Override
    public void showTasks(){
        System.out.println("[TASK LIST]");
        final List<Task> tasks = tasktService.findAll();
        int index = 1;
        for(final Task task: tasks){
            System.out.println(index + ". " + task);
            index++;
        }
        ColorizeConsoleTextUtil.colorPrintln("[OK]", ConsoleColorConst.GREEN);
    }

}
