package ru.t1.zkovalenko.tm.repository;

import ru.t1.zkovalenko.tm.api.ITaskRepository;
import ru.t1.zkovalenko.tm.model.Task;
import java.util.ArrayList;
import java.util.List;

public final class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public List<Task> findAll(){
        return tasks;
    }

    @Override
    public Task add(Task task){
        tasks.add(task);
        return task;
    }

    @Override
    public void clear(){
        tasks.clear();
    }

}
