package ru.t1.zkovalenko.tm.repository;

import ru.t1.zkovalenko.tm.api.ICommandRepository;
import ru.t1.zkovalenko.tm.constant.ArgumentsConst;
import ru.t1.zkovalenko.tm.constant.TerminalConst;
import ru.t1.zkovalenko.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private final static Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentsConst.VERSION,
            "Show app version"
    );

    private final static Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentsConst.ABOUT,
            "Who did it? And why?"
    );

    private final static Command HELP = new Command(
            TerminalConst.HELP, ArgumentsConst.HELP,
            "App commands"
    );

    private final static Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Quit from program"
    );

    private final static Command INFO = new Command(
            TerminalConst.INFO, ArgumentsConst.INFO,
            "Info about system"
    );

    private final static Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentsConst.ARGUMENTS,
            "List of arguments"
    );

    private final static Command COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentsConst.COMMANDS,
            "List of Available commands"
    );

    private final static Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null,
            "Remove all projects"
    );

    private final static Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null,
            "Display project list"
    );

    private final static Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null,
            "Create new project"
    );

    private final static Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null,
            "Remove all tasks"
    );

    private final static Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null,
            "Display task list"
    );

    private final static Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null,
            "Create new task"
    );

    private final static Command[] TERMINAL_CONTROLS = new Command[]{
            INFO, ABOUT, VERSION, HELP, COMMANDS, ARGUMENTS,
            PROJECT_CLEAR, PROJECT_LIST, PROJECT_CREATE,
            TASK_CLEAR, TASK_LIST, TASK_CREATE,
            EXIT
    };

    @Override
    public Command[] getTerminalControls(){
        return TERMINAL_CONTROLS;
    }
}
