package ru.t1.zkovalenko.tm.component;

import ru.t1.zkovalenko.tm.api.*;
import ru.t1.zkovalenko.tm.constant.ArgumentsConst;
import ru.t1.zkovalenko.tm.constant.TerminalConst;
import ru.t1.zkovalenko.tm.controller.CommandController;
import ru.t1.zkovalenko.tm.controller.ProjectController;
import ru.t1.zkovalenko.tm.controller.TaskController;
import ru.t1.zkovalenko.tm.repository.CommandRepository;
import ru.t1.zkovalenko.tm.repository.ProjectRepository;
import ru.t1.zkovalenko.tm.repository.TaskRepository;
import ru.t1.zkovalenko.tm.service.CommandService;
import ru.t1.zkovalenko.tm.service.ProjectService;
import ru.t1.zkovalenko.tm.service.TaskService;
import ru.t1.zkovalenko.tm.util.TerminalUtil;

import java.util.Locale;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITasktService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private void processArgument(final String arg) {
        switch (arg.toLowerCase(Locale.ENGLISH)) {
            case TerminalConst.VERSION:
            case ArgumentsConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.HELP:
            case ArgumentsConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.ABOUT:
            case ArgumentsConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.INFO:
            case ArgumentsConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.ARGUMENTS:
            case ArgumentsConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.COMMANDS:
            case ArgumentsConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTasks();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.EXIT:
                exit();
            default:
                commandController.showError();
        }
    }

    private void exit() {
        System.exit(0);
    }

    public void run (String[] args) {
        if (processArguments(args)) exit();

        commandController.showWelcome();

        while (true) {
            System.out.println("Enter Command:");
            final String command = TerminalUtil.nextLine();
            System.out.println("---");
            processArgument(command);
        }
    }

    private boolean processArguments(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

}
